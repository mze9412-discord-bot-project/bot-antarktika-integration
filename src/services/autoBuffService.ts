// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-AntARKtika-Integration. If not, see <http://www.gnu.org/licenses/>.

import { BotLogger, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { TYPES_ARKVOTINGINTEGRATION, VotingHistoryService } from "@mze9412-discord-bot-project/bot-voting-integration";
import { DeutscheArkServerIdProviderIfc } from "@mze9412-discord-bot-project/bot-voting-integration/dist/interfaces/deutscheArkServerIdProviderIfc";
import axios from "axios";
import { inject, injectable } from "inversify";
import { Logger } from "tslog";
import { AntARKtikaIntegrationConfigDatabaseProvider } from "../database/antarktikaIntegrationConfigDatabaseProvider";
import { AutoBuffData, AutoBuffVoterBuffer } from "../models/autoBuffData";
import { TYPES_ANTARKTIKAINTEGRATION } from "../TYPES";

@injectable()
export class AutoBuffService implements DeutscheArkServerIdProviderIfc {
    private logger!: Logger;

    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private antARKtikaIntegrationConfigDatabaseProvider: AntARKtikaIntegrationConfigDatabaseProvider
        , @inject(TYPES_ARKVOTINGINTEGRATION.VotingHistoryService) private votingHistoryService: VotingHistoryService,
        @inject(TYPES_BOTRUNNER.BotLogger) botLogger: BotLogger
      ) {
          this.logger = botLogger.getChildLogger('AutoBuffService');
    }

    public async getAllBuffData(guildId: string): Promise<AutoBuffData[]> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return [];
        }

        const url = config.serverUrl + `/api/autobuff/get/ForCluster/${config.apiToken}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <AutoBuffData[]>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return [];
    }

    public async getBuffDataForSteamId(guildId: string, steamId: string): Promise<AutoBuffData | null> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return null;
        }

        const url = config.serverUrl + `/api/mod/autobuff/get/forSteamId/${config.apiToken}/${steamId}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <AutoBuffData>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return null;
    }

    public async getBuffDataForDASVotingName(guildId: string, votingUsername: string): Promise<AutoBuffData | null> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return null;
        }

        const url = config.serverUrl + `/api/autoBuff/get/forVotingUsername/${config.apiToken}/${votingUsername}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <AutoBuffData>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return null;
    }
    
    public async updateAdminBuffForSteamId(guildId: string, steamId: string, isAdmin: boolean): Promise<AutoBuffData | null> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return null;
        }

        const url = config.serverUrl + `/api/autoBuff/get/updateAdminBuff/${config.apiToken}/${steamId}/${isAdmin ? 'true' : 'false'}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <AutoBuffData>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return null;
    }
    
    public async updateSupporterBuffForSteamId(guildId: string, steamId: string, isSupporter: boolean): Promise<AutoBuffData | null> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return null;
        }

        const url = config.serverUrl + `/api/autoBuff/get/updateSupporterBuff/${config.apiToken}/${steamId}/${isSupporter ? 'true' : 'false'}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <AutoBuffData>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return null;
    }
    
    public async resetBuffDataForSteamId(guildId: string, steamId: string): Promise<boolean> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return false;
        }

        const url = config.serverUrl + `/api/autoBuff/get/resetBuff/${config.apiToken}/${steamId}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return true;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return false;
    }
    
    public async updateVoterBuffs(guildId: string): Promise<AutoBuffData[]> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return [];
        }

        const date = new Date();
        date.setMonth(date.getMonth() - 1);

        // get voters history for month
        const votersLastMonth = await this.votingHistoryService.getVotingHistoryForMonth(guildId, (date.getMonth() + 1).toString(), date.getFullYear().toString());
        this.logger.debug(`AutoBuff: Voters last month: ${votersLastMonth.length}.`);

        // voters structure
        const voters: AutoBuffVoterBuffer[] = [];
        for (const entry of votersLastMonth) {
            if (entry.votes.length > 0) {
                const voter = new AutoBuffVoterBuffer();
                voter.steamId = entry.steamId;
                voter.bonusAmount = Math.min(entry.votes.length * 0.005, 0.25);
                voters.push(voter);
            }
        }

        const url = config.serverUrl + `/api/autoBuff/post/votersBuff/${config.apiToken}`;
        try {
            const result = await axios.post(url, voters);
            if (result.status === 200) {
                return <AutoBuffData[]>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return [];
    }
    
    public async getUsernameForSteamId(guildId: string, steamId: string): Promise<string> {
        const data = await this.getBuffDataForSteamId(guildId, steamId);
        if (data != null) {
            return data.votingUsernameDAS;
        }

        return '';
    }

    public async setUsernameForSteamId(guildId: string, steamId: string, username: string): Promise<void> {
        await this.setUsernameForSteamId(guildId, steamId, username);
    }

    public async getSteamIdForUsername(guildId: string, username: string): Promise<string> {
        const data = await this.getBuffDataForDASVotingName(guildId, username);
        if (data != null) {
            return data.steamId;
        }

        return '';
    }
}