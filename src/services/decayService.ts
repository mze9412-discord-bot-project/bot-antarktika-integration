// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License

import { BotLogger, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import axios from "axios";
import { inject, injectable } from "inversify";
import { Logger } from "tslog";
import { AntARKtikaIntegrationConfigDatabaseProvider } from "../database/antarktikaIntegrationConfigDatabaseProvider";
import { DecayInfo } from "../models/decayInfo";
import { TYPES_ANTARKTIKAINTEGRATION } from "../TYPES";

@injectable()
export class DecayService {
    private logger!: Logger;
    
    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private antARKtikaIntegrationConfigDatabaseProvider: AntARKtikaIntegrationConfigDatabaseProvider,
        @inject(TYPES_BOTRUNNER.BotLogger) botLogger: BotLogger
        ) {
            this.logger = botLogger.getChildLogger('DecayService');
    }

    public async getTribesWarn(guildId: string): Promise<DecayInfo[]> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return [];
        }

        const url = config.serverUrl + `/api/decay/tribes/warning/${config.apiToken}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <DecayInfo[]>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return [];
    }

    public async getTribesDecay(guildId: string): Promise<DecayInfo[]> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return [];
        }

        const url = config.serverUrl + `/api/decay/tribes/decay/${config.apiToken}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <DecayInfo[]>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return [];
    }

    public async getTribesAnonymous(guildId: string): Promise<DecayInfo[]> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return [];
        }

        const url = config.serverUrl + `/api/decay/tribes/anonymous/${config.apiToken}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <DecayInfo[]>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return [];
    }
}