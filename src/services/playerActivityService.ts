import { inject, injectable } from "inversify";
import { AntARKtikaIntegrationConfigDatabaseProvider } from "../database/antarktikaIntegrationConfigDatabaseProvider";
import { PlayerActivityInfo, QueryData } from "../models/playerActivity";
import { TYPES_ANTARKTIKAINTEGRATION } from "../TYPES";
import axios from "axios";
import { Logger } from "tslog";
import { BotLogger, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { ActivityStatistics } from "../models/activityStatistics";

@injectable()
export class PlayerActivityService {
    private logger!: Logger;
    
    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private antARKtikaIntegrationConfigDatabaseProvider: AntARKtikaIntegrationConfigDatabaseProvider,
        @inject(TYPES_BOTRUNNER.BotLogger) botLogger: BotLogger
        ) {
            this.logger = botLogger.getChildLogger('PlayerActivityService');
    }

    public async queryData(guildId: string, queryTerm: string): Promise<QueryData | null> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return null;
        }

        const url = config.serverUrl + `/api/adminTools/query/${config.apiToken}/${queryTerm}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <QueryData>result.data;
            }
        } catch(ex) {
            this.logger.error(ex);
        }

        return null;
    }

    public async getActivePlayers(guildId: string): Promise<PlayerActivityInfo[]> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return [];
        }

        const url = config.serverUrl + `/api/adminTools/playerActivity/${config.apiToken}`;
        this.logger.debug('Fetching ' + url);
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                this.logger.debug('Result 200');
                let data = <PlayerActivityInfo[]>result.data;
                data = data.sort(x => x.date);
                return data;
            }
        } catch(ex) {
            this.logger.error(ex);
        }

        return [];
    }

    public async getActivityStatistics(guildId: string, date: number): Promise<ActivityStatistics | null> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return null;
        }

        const url = config.serverUrl + `/api/adminTools/activityStatistics/${config.apiToken}/${date}`;
        this.logger.debug('Fetching ' + url);
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                this.logger.debug('Result 200');
                return <ActivityStatistics>result.data;
            }
        } catch(ex) {
            this.logger.error(ex);
        }

        return null;
    }
}