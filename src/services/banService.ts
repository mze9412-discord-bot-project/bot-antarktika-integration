// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-AntARKtika-Integration. If not, see <http://www.gnu.org/licenses/>.

import { BotLogger, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import axios from "axios";
import { inject, injectable } from "inversify";
import { Logger } from "tslog";
import { AntARKtikaIntegrationConfigDatabaseProvider } from "../database/antarktikaIntegrationConfigDatabaseProvider";
import { BanEntry } from "../models/banEntry";
import { TYPES_ANTARKTIKAINTEGRATION } from "../TYPES";

@injectable()
export class BanService {
    private logger!: Logger;

    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private antARKtikaIntegrationConfigDatabaseProvider: AntARKtikaIntegrationConfigDatabaseProvider,
        @inject(TYPES_BOTRUNNER.BotLogger) botLogger: BotLogger
      ) {
          this.logger = botLogger.getChildLogger('BanService');
    }

    public async banPlayer(guildId: string, steamId: string, reason: string): Promise<boolean> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return false;
        }

        const url = config.serverUrl + `/api/banPlayer/${config.apiToken}/${steamId}/${reason}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return true;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return false;
    }

    public async unbanPlayer(guildId: string, steamId: string): Promise<boolean> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return false;
        }

        const url = config.serverUrl + `/api/unbanPlayer/${config.apiToken}/${steamId}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return true;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return false;
    }

    public async getBannedPlayers(guildId: string): Promise<BanEntry[]> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return [];
        }

        const url = config.serverUrl + `/api/bannedPlayers/${config.apiToken}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <BanEntry[]>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return [];
    }
}