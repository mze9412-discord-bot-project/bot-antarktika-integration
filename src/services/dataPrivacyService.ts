// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-AntARKtika-Integration. If not, see <http://www.gnu.org/licenses/>.

import axios from "axios";
import { inject, injectable } from "inversify";
import { AntARKtikaIntegrationConfigDatabaseProvider } from "../database/antarktikaIntegrationConfigDatabaseProvider";
import { TYPES_ANTARKTIKAINTEGRATION } from "../TYPES";
import { PrivacyData } from "../models/privacyData";
import { Logger } from "tslog";
import { BotLogger, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";

@injectable()
export class DataPrivacyService {
    private logger!: Logger;
    
    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private antARKtikaIntegrationConfigDatabaseProvider: AntARKtikaIntegrationConfigDatabaseProvider,
        @inject(TYPES_BOTRUNNER.BotLogger) botLogger: BotLogger
        ) {
            this.logger = botLogger.getChildLogger('DataPrivacyService');
    }

    public async getAllStates(guildId: string): Promise<PrivacyData[]> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return [];
        }

        const url = config.serverUrl + `/api/dataPrivacy/privacyStates/${config.apiToken}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <PrivacyData[]>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return [];
    }
    
    public async getStateForSteamId(guildId: string, steamId: string): Promise<PrivacyData | null> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return null;
        }

        const url = config.serverUrl + `/api/dataPrivacy/privacyState/${config.apiToken}/${steamId}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return <PrivacyData>result.data;
            }
        } catch(ex) {
            this.logger.error(ex)
        }

        return null;
    }
    
    public async revokeAgreement(guildId: string, steamId: string): Promise<boolean> {
        const config = await this.antARKtikaIntegrationConfigDatabaseProvider.get(guildId);
        if (config == null) {
            return false;
        }
        
        const url = config.serverUrl + `/api/dataPrivacy/revoke/${config.apiToken}/${steamId}`;
        try {
            const result = await axios.get(url);
            if (result.status === 200) {
                return true;
            }
        } catch(ex) {
            this.logger.error(ex)
        }
        return false;
    }
}