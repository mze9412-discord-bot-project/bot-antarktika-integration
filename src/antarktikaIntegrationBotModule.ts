// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-AntARKtika-Integration. If not, see <http://www.gnu.org/licenses/>.

import { BotLogger, BotModule, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { inject, injectable } from "inversify";
import { BaseCommandInteraction, CacheType, CommandInteractionOption, CommandInteractionOptionResolver } from "discord.js";
import { SlashCommandBuilder } from "@discordjs/builders";
import { AntARKtikaIntegrationConfig } from "./models/antarktikaIntegrationConfig";
import { TYPES_ANTARKTIKAINTEGRATION } from "./TYPES";
import { AntARKtikaIntegrationConfigDatabaseProvider } from "./database/antarktikaIntegrationConfigDatabaseProvider";
import { AntARKtikaDataprivacyCommand } from "./commands/dataPrivacyCommand";
import { AntARKtikaAutoBuffCommand } from "./commands/autoBuffCommand";
import { AntARKtikaPlayerActivityCommand } from "./commands/playerActivityCommand";
import { AntARKtikaDecayCommand } from "./commands/decayCommand";
import { DataPrivacyService } from "./services/dataPrivacyService";
import { PlayerActivityService } from "./services/playerActivityService";
import { AutoBuffService } from "./services/autoBuffService";
import { DecayService } from "./services/decayService";
import { BanCommand as AntARKtikaBanCommand } from "./commands/banCommand";
import { BanService } from "./services/banService";

@injectable()
export class AntARKtikaIntegrationBotModule extends BotModule {

    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private _configProvider: AntARKtikaIntegrationConfigDatabaseProvider,
        @inject(TYPES_ANTARKTIKAINTEGRATION.DataPrivacyService) private _dataPrivacyService: DataPrivacyService,
        @inject(TYPES_ANTARKTIKAINTEGRATION.AutoBuffService) private _autoBuffService: AutoBuffService,
        @inject(TYPES_ANTARKTIKAINTEGRATION.PlayerActivityService) private _playerActivityService: PlayerActivityService,
        @inject(TYPES_ANTARKTIKAINTEGRATION.DecayService) private _decayService: DecayService,
        @inject(TYPES_ANTARKTIKAINTEGRATION.BanService) private _banService: BanService,
        @inject(TYPES_BOTRUNNER.BotLogger) _botLogger: BotLogger
    ) {
        super('antarktika', 'Integration of AntARKtika server cluster specific services.', _botLogger);
    }

    async initializeCore(): Promise<void> {
        this.addSubCommand(new AntARKtikaDataprivacyCommand(this._dataPrivacyService, this._configProvider));
        this.addSubCommand(new AntARKtikaAutoBuffCommand(this._autoBuffService, this._configProvider));
        this.addSubCommand(new AntARKtikaPlayerActivityCommand(this._configProvider, this._playerActivityService));
        this.addSubCommand(new AntARKtikaDecayCommand(this._configProvider, this._decayService));
        this.addSubCommand(new AntARKtikaBanCommand(this._banService, this._configProvider));
    }
    
    async buildConfigCommand(configCommandBuilder: SlashCommandBuilder): Promise<void> {
        configCommandBuilder.addSubcommand(scmd =>
            scmd.setName(this.name)
            .setDescription('Module specific configuration')

            .addStringOption(opt =>
                opt.setName('apikey')
                .setDescription('What is the API key that the AntARKtika integration API uses for authentication?')
                .setRequired(true)
            )
            .addStringOption(opt =>
                opt.setName('serverurl')
                .setDescription('What is the URL for the AntARKtika API?')
                .setRequired(true)
            )
        );
    }
    
    async handleConfigCommand(interaction: BaseCommandInteraction<CacheType>): Promise<void> {
        if (interaction.guild == null) return;

        const opts = <CommandInteractionOptionResolver>interaction.options;
        const subCommand = <CommandInteractionOption[]>opts.data;
        const options = <CommandInteractionOption[]>subCommand[0].options;
        
        const vals = new AntARKtikaIntegrationConfig();
        vals.guildId = interaction.guild.id;
        
        for(const o of options) {
            if (o.name === 'apikey') vals.apiToken = <string>o.value;
            if (o.name === 'serverurl') vals.serverUrl = <string>o.value;
        
            this.logger.debug(`Configured option: ${o.name}. Value: ${o.value}`);
        }

        await this._configProvider.store(vals, true);
    
        await interaction.reply(`Configuration for ${this.name} saved!`);
    }
    
    public async start(): Promise<void> {
        this.logger.info('AntARKtikaIntegrationBotModule: Start');
    }
    
    public async stop(): Promise<void> {
        this.logger.info('AntARKtikaIntegrationBotModule: Stop');
    }
}