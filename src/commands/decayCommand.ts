// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-AntARKtika-Integration. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CommandInteractionOption, Util } from "discord.js";
import { inject } from "inversify";
import { AntARKtikaIntegrationConfigDatabaseProvider } from "../database/antarktikaIntegrationConfigDatabaseProvider";
import { DecayService } from "../services/decayService";
import { TYPES_ANTARKTIKAINTEGRATION } from "../TYPES";

export class AntARKtikaDecayCommand extends BotCommand {
    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private _configProvider: AntARKtikaIntegrationConfigDatabaseProvider,
        @inject(TYPES_ANTARKTIKAINTEGRATION.DecayService) private _decayService: DecayService
    ) {
        super('decay', 'Handle decay mechanics.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const groupCmdBuilder = new SlashCommandSubcommandGroupBuilder()
            .setName(this.name)
            .setDescription(this.description);

            groupCmdBuilder.addSubcommand(cmd =>
                cmd.setName('listwarn')
                    .setDescription('List all tribes that are in the decay warning list.')
            );
            
            groupCmdBuilder.addSubcommand(cmd =>
                cmd.setName('listdecay')
                    .setDescription('List all tribes that are on the decayed list and ready to be nuked.')
            );
            
            groupCmdBuilder.addSubcommand(cmd =>
                cmd.setName('overview')
                    .setDescription('Get\'s a short decay overview.')
            );
            
        return groupCmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction): Promise<void> {
        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];

        if (subCommand.name === 'listwarn') {
            await this.handleListWarn(interaction);
        } else if (subCommand.name === 'listdecay') {
            await this.handleListDecay(interaction);
        } else if (subCommand.name === 'overview') {
            await this.handleOverview(interaction);
        }
    }

    private async handleOverview(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        await interaction.reply({content: "Fetching decay data. Please wait ...", ephemeral: true});

        const decayTribes = await this._decayService.getTribesDecay(interaction.guild.id);
        const warnTribes = await this._decayService.getTribesWarn(interaction.guild.id);
        const anonTribes = await this._decayService.getTribesAnonymous(interaction.guild.id);
        
        await interaction.followUp(`${decayTribes.length} tribes are in decay.\n${warnTribes.length} tribes are in warning.\n${anonTribes.length} tribes are without known members.`);
    }

    private async handleListDecay(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        await interaction.reply({content: "Fetching decay data. Please wait ...", ephemeral: true});

        const decayTribes = await this._decayService.getTribesDecay(interaction.guild.id);

        const buffer: string[] = [];
        
        buffer.push(`Tribes in Decay: ${decayTribes.length}`);
        let i = 1;
        for (const tribe of decayTribes) {
            const members: string[] = [];
            for(const member of tribe.knownMembers) {
                members.push(`[${member.playerName}**/**${member.steamId}**/**${member.playerId}]`)
            }

            const lastSeenDate = new Date(tribe.lastSeen);
            buffer.push(`${i}) ${tribe.tribeName} (ID: ${tribe.tribeId}) (Map: ${tribe.mapName}) (Members: ${members.join(', ')}) (Last: ${lastSeenDate.toLocaleString()})`);
            i++;
        }

        const split = Util.splitMessage(buffer.join('\n'));
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleListWarn(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        
        await interaction.reply({content: "Fetching decay data. Please wait ...", ephemeral: true});

        const warnTribes = await this._decayService.getTribesWarn(interaction.guild.id);

        const buffer: string[] = [];
        
        buffer.push(`Tribes in Decay Warning: ${warnTribes.length}`);
        let i = 1;
        for (const tribe of warnTribes) {
            const members: string[] = [];
            for(const member of tribe.knownMembers) {
                members.push(`[${member.playerName}**/**${member.steamId}**/**${member.playerId}]`)
            }

            const lastSeenDate = new Date(tribe.lastSeen);
            buffer.push(`${i}) ${tribe.tribeName} (ID: ${tribe.tribeId}) (Map: ${tribe.mapName}) (Members: ${members.join(', ')}) (Last: ${lastSeenDate.toLocaleString()})`);
            i++;
        }

        const split = Util.splitMessage(buffer.join('\n'));
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }
}