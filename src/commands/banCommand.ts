// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-AntARKtika-Integration. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CommandInteractionOption, Util } from "discord.js";
import { inject } from "inversify";
import { AntARKtikaIntegrationConfigDatabaseProvider } from "../database/antarktikaIntegrationConfigDatabaseProvider";
import { BanService } from "../services/banService";
import { TYPES_ANTARKTIKAINTEGRATION } from "../TYPES";

export class BanCommand extends BotCommand {
    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.BanService) private _banService: BanService,
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private _configProvider: AntARKtikaIntegrationConfigDatabaseProvider
    ) {
        super('ban', 'Ban, unban or list banned players.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const groupCmdBuilder = new SlashCommandSubcommandGroupBuilder()
            .setName(this.name)
            .setDescription(this.description);

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('list')
            .setDescription('List all banned users.')
        );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('add')
                .setDescription('Add a player to the banlist.')
                .addStringOption(o =>
                    o.setName('steamid')
                    .setDescription('The player\'s steam id.')
                    .setRequired(true)
                )
                .addStringOption(o =>
                    o.setName('reason')
                    .setDescription('The ban reason.')
                    .setRequired(true)    
                )
            );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('remove')
                .setDescription('Remove a player from the banlist.')
                .addStringOption(o =>
                    o.setName('steamid')
                    .setDescription('The player\'s steam id.')
                    .setRequired(true)
                )
            );
        
        return groupCmdBuilder;
    }
    
    
    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction): Promise<void> {

        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }
        
        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];

        if (subCommand.name === 'list') {
            await this.handleList(interaction);   
        } else if (subCommand.name === 'add') {
            await this.handleBan(interaction);   
        } else if (subCommand.name === 'remove') {
            await this.handleUnban(interaction);   
        }
    }
    
    private async handleList(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if(config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }

        await interaction.reply({ content: 'Fetching data. Stand by ...', ephemeral: true });


        const bans = await this._banService.getBannedPlayers(interaction.guild.id);
        const buffer: string[] = [];
        
        // header
        buffer.push('**Banned list.**');
        buffer.push(`Total bans: ${bans.length}`);
        
        // add content
        let i = 1;
        for (const ban of bans) {
            buffer.push(`${i}) ${ban.steamId} - ${ban.reason} - ${new Date(ban.banDate).toLocaleString()}`);
            i++;
        }

        const split = Util.splitMessage(buffer.join('\n'));
        await interaction.followUp(split[0]);
        for (let i = 1; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }
    
    private async handleBan(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if(config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }

        await interaction.reply({ content: 'Banning player. Stand by ...', ephemeral: true });

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get data
        const steamId = <string>parameters[0].value;
        const reason = <string>parameters[1].value;
        const success = await this._banService.banPlayer(interaction.guild.id, steamId, reason);
        
        // print
        if (success) {
            await interaction.followUp(`SteamId ${steamId} banned!`);
        } else {
            await interaction.followUp(`SteamId ${steamId} could not be banned.`);
        }
    }
    
    private async handleUnban(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if(config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }

        await interaction.reply({ content: 'Unbanning player. Stand by ...', ephemeral: true });

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get data
        const steamId = <string>parameters[0].value;
        const success = await this._banService.unbanPlayer(interaction.guild.id, steamId);
        
        // print
        if (success) {
            await interaction.followUp(`SteamId ${steamId} unbanned!`);
        } else {
            await interaction.followUp(`SteamId ${steamId} could not be unbanned.`);
        }
    }
}