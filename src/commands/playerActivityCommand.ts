// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-AntARKtika-Integration. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CommandInteractionOption, Util } from "discord.js";
import { inject } from "inversify";
import { AntARKtikaIntegrationConfigDatabaseProvider, PlayerActivityService } from "..";
import { TYPES_ANTARKTIKAINTEGRATION } from "../TYPES";

export class AntARKtikaPlayerActivityCommand extends BotCommand {
    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private _configProvider: AntARKtikaIntegrationConfigDatabaseProvider,
        @inject(TYPES_ANTARKTIKAINTEGRATION.PlayerActivityService) private _activityService: PlayerActivityService
    ) {
        super('activity', 'Handle player activity data.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const groupCmdBuilder = new SlashCommandSubcommandGroupBuilder()
            .setName(this.name)
            .setDescription(this.description);

            groupCmdBuilder.addSubcommand(cmd =>
                cmd.setName('list')
                    .setDescription('List all active players from the last 30 days.')
            );
            
            groupCmdBuilder.addSubcommand(cmd =>
                cmd.setName('statistics')
                    .setDescription('Gets activity statistics for the last day (yesterday).')
            );
            
            groupCmdBuilder.addSubcommand(cmd =>
                cmd.setName('query')
                    .setDescription('Query for a name, steamid, etc.')
                    .addStringOption(o =>
                        o.setName('query')
                            .setDescription('The query term, i.e. SteamID, tribe namne, player name, etc..')
                            .setRequired(true)
                    )
            );
            
        return groupCmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction): Promise<void> {
        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];

        if (subCommand.name === 'list') {
            await this.handleList(interaction);
        } else if (subCommand.name === 'statistics') {
            await this.handleStatistics(interaction);
        } else if (subCommand.name === 'query') {
            await this.handleQuery(interaction);
        }
    }

    
    private async handleList(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }

        await interaction.reply({content: "Fetching activity data. Please wait ...", ephemeral: true});

        const activePlayers = await this._activityService.getActivePlayers(interaction.guild.id);
        const sortedPlayers = activePlayers.sort(x => x.date);
                
        const buff: string[] = [];

        if (activePlayers.length == 0) {
            buff.push('Sorry, no active players!');
        } else {
            buff.push(`### Active players in last 30 days: **${sortedPlayers.length}**`);
            for (let i = 0; i < sortedPlayers.length; i++) {
                const data = sortedPlayers[i];
                buff.push(`${i+1}) **${data.playerName}** (ID *${data.playerId}*) (SteamID *${data.steamId}*) (Map *${data.mapName}*) (Last Seen *${(new Date(data.date)).toLocaleString()}*)`);
            }
        }

        const content = buff.join('\n');
        const split = Util.splitMessage(content);
        for (const entry of split) {
            await interaction.followUp(entry);
        }
    }
    
    
    private async handleStatistics(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }

        await interaction.reply({content: "Fetching statistics data. Please wait ...", ephemeral: true});

        // create date
        const date = new Date();
        date.setDate(date.getDate()-1);
        date.setHours(0, 0, 0, 0);

        const statistics = await this._activityService.getActivityStatistics(interaction.guild.id, date.getTime());
        if (statistics == null) {
            await interaction.followUp(`No data for ${date.toLocaleDateString()} found.`);
            return;
        }

        const buff: string[] = [];
        buff.push(`**Statistics for ${date.toLocaleDateString()}**`)
        buff.push(`__Active players:__ ${statistics?.ativePlayers}`);
        buff.push(`__Used maps:__ ${statistics?.usedMaps.join(', ')}`);

        const content = buff.join('\n');
        const split = Util.splitMessage(content);
        for (const entry of split) {
            await interaction.followUp(entry);
        }
    } 
    
    private async handleQuery(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }
        await interaction.reply({content: "Querying database ... Please wait ...", ephemeral: true});
        
        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get query term
        const queryTerm = <string>parameters[0].value;

        // query data
        const activityData = await this._activityService.queryData(interaction.guild.id, queryTerm);
        if (activityData == null) {

            await interaction.reply('Error retrieving query data.');
            return;
        }

        const buff: string[] = [];
        buff.push(`### Query results: Tribes **${activityData?.tribes.length}** / Characters **${activityData?.characters.length}**`);
        
        if (activityData.tribes.length > 0) {
            buff.push('**Tribes**');
            for (let i = 0; i < activityData.tribes.length; i++) {
                const tribe = activityData.tribes[i];
                buff.push(`${i+1}) ${tribe.tribeName} (ID ${tribe.tribeId}) (OwnerId ${tribe.ownerId}) (Map ${tribe.mapName}) (MemberIds ${tribe.memberIds.join(', ')}) (Nuked ${tribe.isNuked ? 'Y' : 'N'})`);
            }
            buff.push('');
        }

        if (activityData.characters.length > 0) {
            buff.push('**Characters**');
            for (let i = 0; i < activityData.characters.length; i++) {
                const char = activityData.characters[i];
                buff.push(`${i+1}) ${char.playerName} (ID ${char.playerId}) (SteamId ${char.steamId}) (TribeId ${char.tribeId}) (Map ${char.mapName})`);
            }
        }

        const content = buff.join('\n');
        const split = Util.splitMessage(content);
        for (const entry of split) {
            await interaction.followUp(entry);
        }
    }
}