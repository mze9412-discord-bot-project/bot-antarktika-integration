// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-AntARKtika-Integration. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CommandInteractionOption, Util } from "discord.js";
import { inject } from "inversify";
import { AntARKtikaIntegrationConfigDatabaseProvider, AutoBuffService } from "..";
import { AutoBuffComponentType, AutoBuffData, AutoBuffStat } from "../models/autoBuffData";
import { TYPES_ANTARKTIKAINTEGRATION } from "../TYPES";

export class AntARKtikaAutoBuffCommand extends BotCommand {
    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.AutoBuffService) private _autoBuffService: AutoBuffService,
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private _configProvider: AntARKtikaIntegrationConfigDatabaseProvider
    ) {
        super('autobuff', 'Administrate AutoBuff settings for players.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const groupCmdBuilder = new SlashCommandSubcommandGroupBuilder()
            .setName(this.name)
            .setDescription(this.description);

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('list')
                .setDescription('List all AutoBuffs.')
        );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('voters')
                .setDescription('Prints list of voters with buff.')
        );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('admins')
                .setDescription('Prints list of admins with buff.')
        );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('supporters')
                .setDescription('Prints list of supporters with buff.')
        );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('info')
                .setDescription('Prints information about one player.')
                .addStringOption(o =>
                    o.setName('steamid')
                        .setDescription('The player\'s steam id.')
                        .setRequired(true)
                )
        );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('updatevoters')
                .setDescription('Updates fixed buffs for voters based on last months voting data.')
        );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('admin')
                .setDescription('Enables or disables admin buff for one player.')
                .addStringOption(o =>
                    o.setName('steamid')
                        .setDescription('The player\'s steam id.')
                        .setRequired(true)
                )
                .addBooleanOption(o =>
                    o.setName("admin")
                        .setDescription('Should the player have the admin buff?')
                        .setRequired(true)
                )
        );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('supporter')
                .setDescription('Enables or disables supporter buff for one player.')
                .addStringOption(o =>
                    o.setName('steamid')
                        .setDescription('The player\'s steam id.')
                        .setRequired(true)
                )
                .addBooleanOption(o =>
                    o.setName("supporter")
                        .setDescription('Should the player have the supporter buff?')
                        .setRequired(true)
                )
        );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('reset')
                .setDescription('Resets auto buff for one player.')
                .addStringOption(o =>
                    o.setName('steamid')
                        .setDescription('The player\'s steam id.')
                        .setRequired(true)
                )
        );

        return groupCmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction): Promise<void> {
        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];

        if (subCommand.name === 'list') {
            await this.handleList(interaction);
        } else if (subCommand.name === 'voters') {
            await this.handleVoters(interaction);
        } else if (subCommand.name === 'admins') {
            await this.handleAdmins(interaction);
        } else if (subCommand.name === 'supporters') {
            await this.handleSupporters(interaction);
        } else if (subCommand.name === 'info') {
            await this.handleInfo(interaction);
        } else if (subCommand.name === 'updatevoters') {
            await this.handleUpdateVoters(interaction);
        } else if (subCommand.name === 'admin') {
            await this.handleAdmin(interaction);
        } else if (subCommand.name === 'reset') {
            await this.handleReset(interaction);
        } else if (subCommand.name === 'supporter') {
            await this.handleSupporter(interaction);
        }
    }

    private async handleUpdateVoters(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }
        await interaction.reply({content: "Updating voters. Please wait ...", ephemeral: true});

        const voters = await this._autoBuffService.updateVoterBuffs(interaction.guild.id);
        
        // print
        const buffer: string[] = []
        if (voters.length === 0) {
            buffer.push(`No one recieved a voter buff for last month's voting!`);
        } else {
            let i = 1;
            buffer.push(`**All voters:**`);
            for (const voter of voters) {
                buffer.push(this.formatBuffForList(voter, i));
                i++;
            }
        }
        
        const split = Util.splitMessage(buffer.join('\n'));
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleInfo(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }
        await interaction.reply({content: "Fetching data. Please wait ...", ephemeral: true});

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get data
        const steamId = <string>parameters[0].value;
        const buff = await this._autoBuffService.getBuffDataForSteamId(interaction.guild.id, steamId);

        // print
        const buffer: string[] = []
        if (buff == null) {
            buffer.push(`Did not find AutoBuff data for SteamId ${steamId}`);
        } else {
            buffer.push(this.formatBuffForList(buff, 1)); // TODO better formatting
        }
        
        const split = Util.splitMessage(buffer.join('\n'));
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleReset(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }
        await interaction.reply({content: "Resetting data. Please wait ...", ephemeral: true});

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get data
        const steamId = <string>parameters[0].value;
        await this._autoBuffService.resetBuffDataForSteamId(interaction.guild.id, steamId);
        
        const split = Util.splitMessage(`Reset all AutoBuff data for ${steamId}`);
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleAdmin(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }
        await interaction.reply({content: "Handling data. Please wait ...", ephemeral: true});

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get data
        const steamId = <string>parameters[0].value;
        const isAdmin = <boolean>parameters[0].value;
        const buff = await this._autoBuffService.updateAdminBuffForSteamId(interaction.guild.id, steamId, isAdmin);
        
        // print
        const buffer: string[] = []
        if (buff == null) {
            buffer.push(`Did not find AutoBuff data for SteamId ${steamId}`);
        } else {
            buffer.push(this.formatBuffForList(buff, 1)); // TODO better formatting
        }
        
        const split = Util.splitMessage(buffer.join('\n'));
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleSupporter(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }
        await interaction.reply({content: "Handling data. Please wait ...", ephemeral: true});

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get data
        const steamId = <string>parameters[0].value;
        const isSupporter = <boolean>parameters[0].value;
        const buff = await this._autoBuffService.updateSupporterBuffForSteamId(interaction.guild.id, steamId, isSupporter);
        
        // print
        const buffer: string[] = []
        if (buff == null) {
            buffer.push(`Did not find AutoBuff data for SteamId ${steamId}`);
        } else {
            buffer.push(this.formatBuffForList(buff, 1)); // TODO better formatting
        }
        
        const split = Util.splitMessage(buffer.join('\n'));
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleList(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }
        await interaction.reply({content: "Handling data. Please wait ...", ephemeral: true});

        const allBuffs = await this._autoBuffService.getAllBuffData(interaction.guild.id);
        const buffer: string[] = [];

        // header
        buffer.push('**Auto Buff list.**');
        buffer.push(`Total Buffs: ${allBuffs.length}`);

        // add content
        let i = 1;
        for (const buff of allBuffs) {
            buffer.push(this.formatBuffForList(buff, i));
            i++;
        }

        const split = Util.splitMessage(buffer.join('\n'));
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleAdmins(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }
        await interaction.reply({content: "Handling data. Please wait ...", ephemeral: true});

        const allBuffs = await this._autoBuffService.getAllBuffData(interaction.guild.id);
        const buffer: string[] = [];

        // header
        buffer.push('**Auto Buff list.**');
        buffer.push(`Total Buffs: ${allBuffs.length}`);

        // add content
        let i = 1;
        for (const buff of allBuffs) {
            let isAdmin = false;
            for (const comp of buff.components) {
                if (comp.type === AutoBuffComponentType.Admin) isAdmin = true;
            }

            // skip if not admin
            if (!isAdmin) continue;

            buffer.push(this.formatBuffForList(buff, i));
            i++;
        }

        const split = Util.splitMessage(buffer.join('\n'));
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleVoters(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }
        await interaction.reply({content: "Handling data. Please wait ...", ephemeral: true});

        const allBuffs = await this._autoBuffService.getAllBuffData(interaction.guild.id);
        const buffer: string[] = [];

        // header
        buffer.push('**Auto Buff list.**');
        buffer.push(`Total Buffs: ${allBuffs.length}`);

        // add content
        let i = 1;
        for (const buff of allBuffs) {
            let isVoter = false;
            for (const comp of buff.components) {
                if (comp.type === AutoBuffComponentType.Voter) isVoter = true;
            }

            // skip if not voter
            if (!isVoter) continue;

            buffer.push(this.formatBuffForList(buff, i));
            i++;
        }

        const split = Util.splitMessage(buffer.join('\n'));
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleSupporters(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        const config = await this._configProvider.get(interaction.guild.id);
        if (config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }
        await interaction.reply({content: "Handling data. Please wait ...", ephemeral: true});

        const allBuffs = await this._autoBuffService.getAllBuffData(interaction.guild.id);
        const buffer: string[] = [];

        // header
        buffer.push('**Auto Buff list.**');
        buffer.push(`Total Buffs: ${allBuffs.length}`);

        // add content
        let i = 1;
        for (const buff of allBuffs) {
            let isSupporter = false;
            for (const comp of buff.components) {
                if (comp.type === AutoBuffComponentType.Supporter) isSupporter = true;
            }

            // skip if not supporter
            if (!isSupporter) continue;

            buffer.push(this.formatBuffForList(buff, i));
            i++;
        }

        const split = Util.splitMessage(buffer.join('\n'));
        for (let i = 0; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private formatBuffForList(buff: AutoBuffData, entryIndex: number): string {
        // calculate value
        let bonus = 0;
        let isAdmin = false;
        let isVoter = false;
        let isSupporter = false;
        for (const comp of buff.components) {
            bonus += comp.bonusValue;
            if (comp.type === AutoBuffComponentType.Admin) isAdmin = true;
            if (comp.type === AutoBuffComponentType.Voter) isVoter = true;
            if (comp.type === AutoBuffComponentType.Supporter) isSupporter = true;
        }
        return `${entryIndex}) ${buff.steamId} - ${bonus * 100}% on ${this.statToString(buff.stat)} - Admin: ${isAdmin ? 'Y' : 'N'} - Supporter: ${isSupporter ? 'Y' : 'N'} - Voter: ${isVoter ? 'Y' : 'N'}`;
    }

    private statToString(stat: AutoBuffStat): string {
        switch (stat) {
            case AutoBuffStat.Food:
                return 'Food';
            case AutoBuffStat.Fortitude:
                return 'Fortitude';
            case AutoBuffStat.Health:
                return 'Health';
            case AutoBuffStat.Melee:
                return 'Melee';
            case AutoBuffStat.Oxygen:
                return 'Oxygen';
            case AutoBuffStat.Speed:
                return 'Speed';
            case AutoBuffStat.Stamina:
                return 'Stamina';
            case AutoBuffStat.Water:
                return 'Water';
            case AutoBuffStat.Weight:
                return 'Weight';
            case AutoBuffStat.XP:
                return 'XP';
            default:
                return 'N/A';
        }
    }
}