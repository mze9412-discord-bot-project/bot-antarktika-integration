// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-AntARKtika-Integration. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CommandInteractionOption, Util } from "discord.js";
import { inject } from "inversify";
import { AntARKtikaIntegrationConfigDatabaseProvider, DataPrivacyService } from "..";
import { TYPES_ANTARKTIKAINTEGRATION } from "../TYPES";

export class AntARKtikaDataprivacyCommand extends BotCommand {
    constructor(
        @inject(TYPES_ANTARKTIKAINTEGRATION.DataPrivacyService) private _dataPrivacyService: DataPrivacyService,
        @inject(TYPES_ANTARKTIKAINTEGRATION.AntARKtikaIntegrationConfigDatabaseProvider) private _configProvider: AntARKtikaIntegrationConfigDatabaseProvider
    ) {
        super('dataprivacy', 'Get an insight into data privacy statistics and agreements.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const groupCmdBuilder = new SlashCommandSubcommandGroupBuilder()
            .setName(this.name)
            .setDescription(this.description);

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('list')
            .setDescription('List all users who agreed to the terms and conditions.')
        );
        
        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('statistics')
            .setDescription('Print some statistics about data privacy agreements.')
        );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('info')
                .setDescription('Print information about one player.')
                .addStringOption(o =>
                    o.setName('steamid')
                    .setDescription('The player\'s steam id.')
                    .setRequired(true)    
                )
            );

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('revoke')
                .setDescription('Revoke the dataprivacy agreement for a player.')
                .addStringOption(o =>
                    o.setName('steamid')
                    .setDescription('The player\'s steam id.')
                    .setRequired(true)    
                )
            );
            
        return groupCmdBuilder;
    }
    
    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction): Promise<void> {

        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }
        
        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];

        if (subCommand.name === 'list') {
            await this.handleList(interaction);   
        } else if (subCommand.name === 'info') {
            await this.handleInfo(interaction);   
        } else if (subCommand.name === 'revoke') {
            await this.handleRevoke(interaction);   
        } else if (subCommand.name === 'statistics') {
            await this.handleStatistics(interaction);
        }
    }
    
    private async handleList(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if(config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }

        const allStates = await this._dataPrivacyService.getAllStates(interaction.guild.id);
        const buffer: string[] = [];
        
        // header
        buffer.push('**Data Privacy Agreements list.**');
        buffer.push(`Total agreements: ${allStates.length}`);
        
        // add content
        let i = 1;
        for (const state of allStates) {
            buffer.push(`${i}) ${state.steamId} - ${state.privacyState}`);
            i++;
        }

        const split = Util.splitMessage(buffer.join('\n'));
        await interaction.reply(split[0]);
        for (let i = 1; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }
    
    private async handleInfo(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if(config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get data
        const steamId = <string>parameters[0].value;
        const data = await this._dataPrivacyService.getStateForSteamId(interaction.guild.id, steamId);
        
        // print
        const buffer: string[] = [];
        buffer.push(`SteamId ${steamId} agreement state: ${data ? data.privacyState : 'null'}`);

        const split = Util.splitMessage(buffer.join('\n'));
        await interaction.reply(split[0]);
        for (let i = 1; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }
    
    private async handleRevoke(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if(config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get data
        const steamId = <string>parameters[0].value;
        const success = await this._dataPrivacyService.revokeAgreement(interaction.guild.id, steamId);
        const data = await this._dataPrivacyService.getStateForSteamId(interaction.guild.id, steamId);

        // print
        const buffer: string[] = [];
        buffer.push(`SteamId ${steamId} agreement revocation ${success ? 'successful' : 'failed'}.`);
        buffer.push(`Current state: ${data ? data.privacyState : 'null'}.`);

        const split = Util.splitMessage(buffer.join('\n'));
        await interaction.reply(split[0]);
        for (let i = 1; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }
    
    private async handleStatistics(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const config = await this._configProvider.get(interaction.guild.id);
        if(config == null) {
            await interaction.reply('You must configure this module first!');
            return;
        }

        const allStates = await this._dataPrivacyService.getAllStates(interaction.guild.id);
        const buffer: string[] = [];
        
        // header
        buffer.push('**Data Privacy Agreements list.**');
        buffer.push(`Total agreements: ${allStates.length}`);

        const split = Util.splitMessage(buffer.join('\n'));
        await interaction.reply(split[0]);
        for (let i = 1; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }
}