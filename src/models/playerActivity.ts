// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-AntARKtika-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-antarktika-integration>.
//
// Bot-AntARKtika-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-AntARKtika-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-AntARKtika-Integration. If not, see <http://www.gnu.org/licenses/>.

import { v4 as uuidv4 } from 'uuid';

export class QueryData {
    tribes: TribeData[] = [];
    characters: CharacterData[] = [];
}

export class TribeData {
    constructor() {
        this.id = uuidv4();
    }

    mod = 'adminTools'
    key = '';
    id = '';
    
    mapName = '';
    sessionName = '';

    tribeId = 0;
    ownerId = 0;
    tribeName = '';
    memberIds: number[] = [];

    isNuked = false;
}

export class CharacterData {
    constructor() {
        this.id = uuidv4();
    }

    mod = 'adminTools';
    key = '';
    id = '';
    
    mapName = '';
    sessionName = '';
    
    playerId = '';
    steamId = '';
    playerName = '';
    isInTribe = false;
    tribeId = '';
    
    isNuked = false;
}

export class PlayerActivityInfo {
    constructor() {
        this.id = uuidv4();
    }

    key = '';
    id = '';

    steamId = '';
    playerName = '';
    playerId = '';
    date = -1;

    mapName = '';
    sessionName = '';
}